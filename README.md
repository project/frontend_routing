## INTRODUCTION

The Frontend Routes module is a small helper to map frontend routes to Drupal routes. 
It is useful when you want to use a frontend framework like Nuxt or Vue.js and want to use Drupal as a backend.

The primary use case for this module is:

- Map frontend routes to Drupal routes using keys
- Assign nodes to these routes
- Use the node data in the frontend

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Go to `/admin/config/services/frontend-routing` and add a new keyed route.
- Your frontend framework can also write a frontend_routing.settings.yml file in the drupal config directory.

## MAINTAINERS

Current maintainers for Drupal 10:

- ayalon - https://www.drupal.org/u/ayalon

