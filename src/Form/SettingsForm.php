<?php

declare(strict_types=1);

namespace Drupal\frontend_routing\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\State\StateInterface;
use Drupal\pathauto\AliasStorageHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Frontend routing settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ConfigFactoryInterface $config_factory,
    protected StateInterface $state,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected AliasStorageHelperInterface $aliasStorageHelper,
    TypedConfigManagerInterface $typedConfigManager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('pathauto.alias_storage_helper'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'frontend_routing_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['frontend_routing.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('frontend_routing.settings');
    $nodes = $this->state->get('frontend_routing.settings') ?: [];
    $bundles = $this->state->get('frontend_routing.bundles') ?: [];

    $keys = $config->get('keys') ?: [];

    $form['existing'] = [
      '#type' => 'details',
      '#title' => $this->t('Existing mappings'),
      '#open' => TRUE,
    ];

    $headers = [
      $this->t('Key'),
      $this->t('Node'),
      $this->t('Language'),
      $this->t('Alias'),
    ];

    $form['existing']['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#tree' => TRUE,
      '#empty' => $this->t('No mappings found.'),
    ];

    foreach ($keys as $key => $value) {
      $aliases = $value['aliases'] ?? [];

      $form['existing']['table'][$key]['key'] = [
        '#title' => $this->t('Key'),
        '#type' => 'textfield',
        '#default_value' => $key,
        '#disabled' => TRUE,
      ];

      $required = TRUE;
      // If we have a generic alias, we do not require a node.
      if (!empty($aliases)) {
        $alias = reset($aliases);
        if (!empty($alias) && strpos($alias, '/:slug') !== FALSE) {
          $required = FALSE;
        }
      }

      $form['existing']['table'][$key]['entity_id'] = [
        '#title' => $this->t('Node'),
        '#type' => 'entity_autocomplete',
        '#target_type' => 'node',
        '#required' => FALSE,
        '#disabled' => !$required,
        '#default_value' => !empty($nodes[$key]) ? $this->entityTypeManager->getStorage('node')->load($nodes[$key]) : NULL,
      ];

      $form['existing']['table'][$key]['alias'] = [
        '#title' => $this->t('Aliases'),
        '#type' => 'item',
        // '#default_value' => $alias,
        '#markup' => implode(',', $aliases),
      ];

      $form['existing']['table'][$key]['delete'] = [
        '#value' => $this->t('Delete'),
        '#type' => 'submit',
        '#limit_validation_errors' => [],
        '#submit' => [
          [$this, 'deleteEntry'],
        ],
      ];

    }

    $form['bundles'] = [
      '#title' => $this->t('Node Types managed by frontend routing'),
      '#type' => 'checkboxes',
      '#options' => $this->getNodeTypes(),
      '#default_value' => !empty($bundles) ? $bundles : [],
    ];

    $form['details'] = [
      '#type' => 'details',
      '#title' => $this->t('Add new mapping'),
      '#open' => TRUE,
    ];

    $form['details']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#description' => 'Unique key to identify the mapping.',
      '#maxlength' => 255,
    ];

    $form['details']['entity_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Node'),
      '#target_type' => 'node',
      '#maxlength' => 255,
    ];

    $form['details']['language'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Language'),
      '#languages' => LanguageInterface::STATE_CONFIGURABLE,
    ];

    $form['details']['alias'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alias'),
      '#description' => 'Starts with a /.',
      '#maxlength' => 255,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    if ($form_state->getValue('key')) {
      if (empty($form_state->getValue('bundle'))) {
        $form_state->setErrorByName('bundle', $this->t('Node Type required.'));
      }
      if (empty($form_state->getValue('entity_id'))) {
        $form_state->setErrorByName('entity_id', $this->t('Node is required.'));
      }
      if (empty($form_state->getValue('language'))) {
        $form_state->setErrorByName('language', $this->t('Language is required.'));
      }
      if (empty($form_state->getValue('alias'))) {
        $form_state->setErrorByName('alias', $this->t('Alias is required.'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * Deletes an entry from the frontend_routing settings.
   *
   * @param array &$form
   *   The form array passed by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object containing the user input and other data.
   *
   * @throws \Drupal\Core\Config\ConfigException
   *   In case of configuration errors.
   */
  public function deleteEntry(array &$form, FormStateInterface $form_state): void {
    $table = $form_state->getValue('table');
    $keys = $this->config('frontend_routing.settings')->get('keys') ?? [];

    // Remove the selected keys.
    foreach ($table as $key => $value) {
      unset($keys[$key]);
    }
    $this->config('frontend_routing.settings')
      ->set('keys', $keys)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $nodes = [];
    // Save data from the table.
    $keys = $this->config('frontend_routing.settings')->get('keys') ?? [];

    // Add a new item.
    $values = $form_state->getValues();
    if (!empty($values['key'])) {
      $key = !empty($keys[$values['key']]) ? $keys[$values['key']] : [];
      $aliases = $key['aliases'] ?? [];
      // Prepend alias with / if it does not have one.
      if (strpos($values['alias'], '/') !== 0) {
        $values['alias'] = '/' . $values['alias'];
      }
      $aliases[$values['language']] = $values['alias'];

      $key['aliases'] = $aliases;
      $keys[$values['key']] = $key;

      $nodes[$values['key']] = $values['entity_id'];

      $key['bundle'] = $values['bundle'];

      $this->config('frontend_routing.settings')
        ->set('keys', $keys)
        ->save();
    }

    $table = $form_state->getValue('table');
    if (is_array($table)) {
      foreach ($table as $key => $value) {
        if (!empty($value['entity_id'])) {
          $nodes[$key] = $value['entity_id'];
        }

      }
      $this->config('frontend_routing.settings')
        ->set('keys', $keys)->save();
    }

    $this->updateNodeAliasSetting($nodes);
    $this->state->set('frontend_routing.bundles', $values['bundles']);
    parent::submitForm($form, $form_state);

  }

  /**
   * Update the pathauto setting for nodes in the frontend_routing.settings state.
   */
  private function updateNodeAliasSetting(array $nodes) {
    $this->state->set('frontend_routing.settings', $nodes);
    $config = $this->config('frontend_routing.settings')->get('keys') ?? [];
    $nodes = $this->state->get('frontend_routing.settings') ?? [];

    // Update the alias for all related nodes.
    foreach ($nodes as $key => $node_id) {
      if ($node_id) {
        $node = $this->entityTypeManager->getStorage('node')->load($node_id);
        $aliases = !empty($config[$key]['aliases']) ? $config[$key]['aliases'] : [];

        // First delete the existing alias.
        $path = '/' . $node->toUrl()->getInternalPath();
        $results = $this->entityTypeManager->getStorage('path_alias')->loadByProperties(['path' => $path]);
        foreach ($results as $result) {
          $result->delete();
        }

        // Iterate through all languages and check if we have a translation.
        foreach ($aliases as $language => $alias) {
          if ($node->hasTranslation($language)) {
            $node = $node->getTranslation($language);
          }
          else {
            continue;
          }
          $paths = $node->get('path')->getValue();

          foreach ($paths as &$path) {
            if ($path['langcode'] === $language) {
              // Save the alias and set pathauto to false.
              $alias = preg_replace('/^\/' . $language . '/', '', $alias);
              $path['alias'] = $alias;
              $path['pathauto'] = FALSE;
              $this->updateAlias($path, $node);
            }
          }
        }
      }
    }
  }

  /**
   * Get Node bundles.
   *
   * @return array
   *   List of node bundles.
   */
  private function getNodeTypes() {
    $list = [];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('node');
    foreach ($bundles as $key => $bundle) {
      $list[$key] = $bundle['label'];
    }
    return $list;
  }

  private function updateAlias($path, $entity) {
    $path_alias_storage = $this->entityTypeManager->getStorage('path_alias');
    $alias_langcode = $path['langcode'];

    // If we have an alias, we need to create or update a path alias entity.
    if ($path['alias']) {
      if (empty($path['pid'])) {
        $path_alias = $path_alias_storage->create([
          'path' => '/' . $entity->toUrl()->getInternalPath(),
          'alias' => $path['alias'],
          'langcode' => $alias_langcode,
        ]);
        $path_alias->save();
        $path['pid'] = $path_alias->id();
      }
      elseif ($path['pid']) {
        $path_alias = $path_alias_storage->load($path['pid']);

        if ($path['alias'] != $path_alias->getAlias()) {
          $path_alias->setAlias($path['alias']);
          $path_alias->save();
        }
      }
    }
  }

}
